import ForgeUI, { Fragment, Text, Select, Option } from '@forge/ui';

const Rating = ({ name, label, description }) => {
  return (
    <Fragment>
      <Select label={label} name={name} description={description}>
        <Option defaultSelected label="1" value={1} />
        <Option label="2" value={2} />
        <Option label="3" value={3} />
        <Option label="4" value={4} />
        <Option label="5" value={5} />
        <Option label="6" value={6} />
        <Option label="7" value={7} />
        <Option label="8" value={8} />
        <Option label="9" value={9} />
        <Option label="10" value={10} />
      </Select>
      <Text>{description}</Text>
      <Text>{'  '}</Text>
    </Fragment>
  );
};

export default Rating;
