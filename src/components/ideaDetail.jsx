import ForgeUI, {
  Fragment,
  Text,
  Button,
  ButtonSet,
  ModalDialog,
  Image,
  Table,
  Row,
  Avatar,
  Cell,
} from '@forge/ui';

const IdeaDetail = ({
  currentSelectedIdea,
  setisIdeaDetailOpen,
  isIdeaDetailOpen,
  setCurrentSelectedIdea,
}) => {
  const {
    idea,
    impact,
    confidence,
    ease,
    image,
    liked,
    accountId,
    description,
  } = currentSelectedIdea;
  return (
    <Fragment>
      {isIdeaDetailOpen && (
        <ModalDialog
          header="Idea details"
          onClose={() => {
            setisIdeaDetailOpen(false);
          }}
        >
          <Image src={image} alt={idea} />
          {/* For spacing starts here */}
          <Text>{'  '}</Text>
          <Text>{'  '}</Text>
          {/* For spacing ends */}
          <Table>
            <Row>
              <Cell>
                <Text content="**Idea:**" />
              </Cell>
              <Cell>
                <Text content={idea} />
              </Cell>
            </Row>
            <Row>
              <Cell>
                <Text content="**description:**" />
              </Cell>
              <Cell>
                <Text content={description} />
              </Cell>
            </Row>
            <Row>
              <Cell>
                <Text content="**Impact:**" />
              </Cell>
              <Cell>
                <Text content={`${impact}`} />
              </Cell>
            </Row>
            <Row>
              <Cell>
                <Text content="**Confidence:**" />
              </Cell>
              <Cell>
                <Text content={`${confidence}`} />
              </Cell>
            </Row>
            <Row>
              <Cell>
                <Text content="**Ease:**" />
              </Cell>
              <Cell>
                <Text content={`${ease}`} />
              </Cell>
            </Row>
            <Row>
              <Cell>
                <Text content="**Creator:**" />
              </Cell>
              <Cell>
                <Avatar accountId={accountId} />
              </Cell>
            </Row>
          </Table>
          <ButtonSet>
            <Button
              text={liked ? '🧡 liked' : '💚 like'}
              onClick={() =>
                setCurrentSelectedIdea({
                  ...currentSelectedIdea,
                  liked: !liked,
                })
              }
            />
            <Button
              text="Create Jira story"
              onClick={() => console.log('perform button 2 click action')}
            />
          </ButtonSet>
        </ModalDialog>
      )}
    </Fragment>
  );
};

export default IdeaDetail;
