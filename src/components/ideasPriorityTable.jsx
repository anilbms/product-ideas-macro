import ForgeUI, { Fragment, Text, Table, Row, Cell, Head } from '@forge/ui';

const IdeasPriorityTable = ({ ideas }) => {
  return (
    <Fragment>
      <Table>
        <Head>
          <Cell>
            <Text content="Idea" />
          </Cell>
          <Cell>
            <Text content="Impact" />
          </Cell>
          <Cell>
            <Text content="Confidence" />
          </Cell>
          <Cell>
            <Text content="Ease" />
          </Cell>
          <Cell>
            <Text content="Score" />
          </Cell>
        </Head>
        {ideas.map(({ idea, impact, confidence, ease }) => (
          <Row>
            <Cell>
              <Text content={`${idea}`} />
            </Cell>
            <Cell>
              <Text content={`${impact}`} />
            </Cell>
            <Cell>
              <Text content={`${confidence}`} />
            </Cell>
            <Cell>
              <Text content={`${ease}`} />
            </Cell>
            <Cell>
              <Text content={`${impact * ease * confidence}`} />
            </Cell>
          </Row>
        ))}
      </Table>
    </Fragment>
  );
};

export default IdeasPriorityTable;
