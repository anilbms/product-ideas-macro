import ForgeUI, {
  Fragment,
  Form,
  TextField,
  TextArea,
  ModalDialog,
  Text,
  Select,
  Option,
} from '@forge/ui';

const Rating = ({ name, label, description }) => {
  return (
    <Fragment>
      <Select label={label} name={name} description={description}>
        <Option defaultSelected label="1" value={1} />
        <Option label="2" value={2} />
        <Option label="3" value={3} />
        <Option label="4" value={4} />
        <Option label="5" value={5} />
        <Option label="6" value={6} />
        <Option label="7" value={7} />
        <Option label="8" value={8} />
        <Option label="9" value={9} />
        <Option label="10" value={10} />
      </Select>
      <Text>{description}</Text>
      <Text>{'  '}</Text>
    </Fragment>
  );
};

const AddProductIdea = ({
  isAddIdeaDialogOpen,
  setSubmitted,
  setIsAddIdeaDialogOpen,
}) => {
  return (
    <Fragment>
      {isAddIdeaDialogOpen && (
        <ModalDialog
          header="Add your idea 💡"
          onClose={() => setIsAddIdeaDialogOpen(false)}
        >
          <Form onSubmit={setSubmitted}>
            <TextField
              label="What's your great idea? 💡"
              name="idea"
              description="Tell your idea which brings value to our users and bring business value."
            />
            <TextArea label="Description" name="description" />
            <Rating
              label="Business impact of the idea? 💼"
              name="impact"
              description="1 is just solve problem of the specific customer, 8 is game changer for the company"
            />
            <Rating
              label="How confident are you with your idea? 👍"
              name="confidence"
              description="1 is just based on product data points, 8 is based on the MVP launch results"
            />
            <Rating
              label="How ease to implement? 🔨"
              name="ease"
              description="1 is more than 6months to implement, 10 is less than one week to implement"
            />
          </Form>
        </ModalDialog>
      )}
    </Fragment>
  );
};

export default AddProductIdea;
