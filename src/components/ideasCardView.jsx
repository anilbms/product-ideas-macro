import ForgeUI, {
  Fragment,
  Text,
  Button,
  Image,
  Table,
  Row,
  Cell,
} from '@forge/ui';

import { chunkArray } from '../util';

const IdeasCardView = ({
  ideas,
  setCurrentSelectedIdea,
  setisIdeaDetailOpen,
}) => {
  return (
    <Fragment>
      {ideas.length && (
        <Table>
          {chunkArray(ideas, 3).map((rowItems) => (
            <Row>
              {rowItems.map(
                ({
                  image,
                  idea,
                  description,
                  impact,
                  confidence,
                  ease,
                  accountId,
                }) => (
                  <Cell>
                    <Image src={image} alt={idea} />
                    <Text> **{idea}**</Text>
                    <Text>{description}</Text>
                    <Button
                      text="more info ➡️"
                      onClick={() => {
                        setCurrentSelectedIdea({
                          image,
                          idea,
                          description,
                          impact,
                          confidence,
                          ease,
                          accountId,
                        });
                        setisIdeaDetailOpen(true);
                      }}
                    />
                    <Text>{`\n`}</Text>
                    <Text>{`\n`}</Text>
                  </Cell>
                )
              )}
            </Row>
          ))}
        </Table>
      )}
    </Fragment>
  );
};

export default IdeasCardView;
