import API from '@forge/api';
import {
    getRandomColor,
    THUMBNAIL_DIMENSION
} from './util';

const AIRTABLE_API_KEY = process.env.AIRTABLE_API_KEY;
const ADD_IDEA_TO_AIRTABLE_URL = 'https://api.airtable.com/v0/apphjayW8FoJHUQQw/Product%20ideas';
const GET_PRODUCT_IDEAS_FROM_AIRTABLE_URL = 'https://api.airtable.com/v0/apphjayW8FoJHUQQw/Product%20ideas?view=Grid%20view';
const PLACEHOLDER_URL = `https://via.placeholder.com/${THUMBNAIL_DIMENSION}/${getRandomColor()}/FFFFFF`;

const headers = {
    Authorization: `Bearer ${AIRTABLE_API_KEY}`,
    'Content-Type': 'application/json',
};


export const getIdeasFromAirtable = async () => API.fetch(
        GET_PRODUCT_IDEAS_FROM_AIRTABLE_URL, {
            headers
        }
    )
    .then((response) => response.json())
    .then((data) => data.records.map((record) => ({
        ...record.fields
    })))
    .catch((error) => {
        console.error('Error:', error);
        return [];
    })


export const addIdeaToAirtable = async (fields) => {
    const newIdea = {
        ...fields,
        image: `${PLACEHOLDER_URL}?text=${fields.idea}`,
    };
    await API.fetch(
            ADD_IDEA_TO_AIRTABLE_URL, {
                headers,
                method: 'POST',
                body: JSON.stringify({
                    records: [{
                        fields: {
                            ...newIdea
                        },
                    }, ],
                }),
            }
        )
        .then((response) => response.json())
        .catch((error) => {
            console.error('Error:', error);
        });
    return newIdea;
}