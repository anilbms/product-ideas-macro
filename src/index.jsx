import ForgeUI, {
  render,
  Fragment,
  Macro,
  Button,
  ButtonSet,
  useState,
  useAction,
  useProductContext,
} from '@forge/ui';

import IdeasPriorityTable from './components/ideasPriorityTable';
import IdeasCardView from './components/ideasCardView';
import AddProductIdea from './components/addProductIdea';
import IdeaDetail from './components/ideaDetail';
import { sortIdeasByScore } from './util';
import { getIdeasFromAirtable, addIdeaToAirtable } from './api';

const App = () => {
  const [isIdeaDetailOpen, setisIdeaDetailOpen] = useState(false);
  const [isAddIdeaDialogOpen, setIsAddIdeaDialogOpen] = useState(false);
  const [isSortByPriority, setIsSortByPriority] = useState(false);
  const [currentSelectedIdea, setCurrentSelectedIdea] = useState({});
  const [ideas, setIdeas] = useState(async () => await getIdeasFromAirtable());

  const { accountId } = useProductContext();
  // useAction is a Forge UI hook we use to manage the form data in local state
  const [submitted, setSubmitted] = useAction(async (_, fields) => {
    const newIdea = await addIdeaToAirtable({ ...fields, accountId });
    setIdeas([...ideas, newIdea]);
    setIsAddIdeaDialogOpen(false);
    return fields;
  }, undefined);

  return (
    <Fragment>
      <ButtonSet>
        <Button
          text="➕ Add product idea"
          onClick={() => {
            setIsAddIdeaDialogOpen(true);
          }}
        />
        <Button
          text="🔁 Toggel sort by priority"
          onClick={() => {
            if (!isSortByPriority) {
              setIdeas(sortIdeasByScore(ideas));
            }
            setIsSortByPriority(!isSortByPriority);
          }}
        />
      </ButtonSet>
      {!isIdeaDetailOpen && !isAddIdeaDialogOpen && !isSortByPriority && (
        <IdeasCardView
          ideas={ideas}
          setCurrentSelectedIdea={setCurrentSelectedIdea}
          setisIdeaDetailOpen={setisIdeaDetailOpen}
        />
      )}
      {!isIdeaDetailOpen && !isAddIdeaDialogOpen && isSortByPriority && (
        <IdeasPriorityTable ideas={ideas} />
      )}
      <IdeaDetail
        currentSelectedIdea={currentSelectedIdea}
        setisIdeaDetailOpen={setisIdeaDetailOpen}
        setCurrentSelectedIdea={setCurrentSelectedIdea}
        isIdeaDetailOpen={isIdeaDetailOpen}
      />
      <AddProductIdea
        isAddIdeaDialogOpen={isAddIdeaDialogOpen}
        setSubmitted={setSubmitted}
        setIsAddIdeaDialogOpen={setIsAddIdeaDialogOpen}
      />
    </Fragment>
  );
};

export const run = render(<Macro app={<App />} />);
