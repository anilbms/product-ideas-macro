/**
 * Sort given array base on it's item property value
 * @param {array} records 
 */
export const sortIdeasByScore = (records) =>
    records
    .sort(
        ({
            impact: impact1,
            ease: ease1,
            confidence: confidence1
        }, {
            impact: impact2,
            ease: ease2,
            confidence: confidence2
        }) => impact1 * ease1 * confidence1 - impact2 * ease2 * confidence2
    )
    .reverse();


/**
 * Split one dimentional array into two diemntional array.
 * Need to create a layout using table cell.
 *
 * @param {array} arr array of items
 * @param {number} chunkCount number of items in a row
 */
export const chunkArray = (arr, chunkCount) => {
    let tempArr = [...arr];
    let chunks = [];
    let chunkSize = Math.floor(tempArr.length / chunkCount);

    while (chunkSize > -1 && tempArr.length) {
        let chunk = tempArr.splice(0, chunkCount);
        chunks.push(chunk);
        chunkSize--;
    }
    return chunks;
};

// Feel free to change this and get some more inspiration from here https://color.adobe.com/
const COLORS = ['5E308C', '049DD9', '04BFAD', '8C7669', 'A64941'];
export const getRandomColor = () => COLORS[Math.floor(Math.random() * COLORS.length)];

export const THUMBNAIL_DIMENSION = '300x150';