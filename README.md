# Product Ideas Forge macro app

This project contains a Forge macro app written in Javascript that displays **Product ideas** by team members in a Confluence macro. **Airtable** is used as a database to store the ideas.

## Requirements

You need the following:

- [Airtable account](https://nodejs.org/en/download/)
- [Airtable base get all records URL](https://nodejs.org/en/download/)
- [Airtable base add a record URL](https://nodejs.org/en/download/)

## Airtable

- Create [Airtable base](https://www.airtable.com/), it's FREE! and duplicate this [Airtable base](https://airtable.com/shrmfUFoU7h3nGsFs).

- Go to [Airtable API](https://airtable.com/api) and find your recently create Airtable base. You will find api key and authentification information and update your Airtable specific information in `src/api.js`.

```javascript
const AIRTABLE_API_KEY = '<YOUR_AIRTABLE_API_KEY>';
const ADD_IDEA_TO_AIRTABLE_URL = '<YOUR_AIRTABLE_BASE_CREATE_RECORD_URL>';
const GET_PRODUCT_IDEAS_FROM_AIRTABLE_URL =
  '<YOUR_AIRTABLE_BASE_GET_RECORDS_URL>';
```

## Quick demo

[![Product ideas Forge app macro in confluence](http://img.youtube.com/vi/mDSV9drO5P0/0.jpg)](https://youtu.be/mDSV9drO5P0)

## Quick start

- Install dependencies by running:

  ```
  npm install
  ```

- Modify your app by editing the `src/api.tsx` file and your Airtable base specific key and urls.

- Build and deploy your app by running:

  ```
  forge deploy
  ```

- Install your app in an Atlassian site by running:

  ```
  forge install
  ```

### Notes

- Deploy your app, with the `forge deploy` command, any time you make changes to the code.
- Install your app, with the `forge install` command, when you want to install your app on a new site. Once the app is installed on a site, the site picks up the new app changes you deploy without needing to run the install command again.

## Support

See [Get help](https://developer.atlassian.com/platform/forge/get-help/) for how to get help and provide feedback.
If you have any questions or feedback, contact [Anil Kumar Krishnashetty](mailto:anilbms75@gmail.com) or on Twitter [@anilbms75](https://twitter.com/anilbms75).
